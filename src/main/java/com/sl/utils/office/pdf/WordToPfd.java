package com.sl.utils.office.pdf;

import com.lowagie.text.Font;
import com.lowagie.text.pdf.BaseFont;
import fr.opensagres.xdocreport.itext.extension.font.IFontProvider;
import org.apache.commons.io.IOUtils;
import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.awt.*;
import java.io.*;

/**
 * poi和poi-ooxml 3.14才可以 打开pom.xml中的3.14版本依赖，注释3.17版本
 * @Version: 1.0
 */
public class WordToPfd {
    public static void main(String[] args) throws Exception {
//        String outPath = "R:\\work\\大数据报告.docx";
//        wordConverterToPdf(outPath, outPath.replace("docx", "pdf"), "C:/Windows/Fonts/simsunb.ttf", BaseFont.IDENTITY_H);
//        wordConverterToPdf(outPath, outPath.replace("docx", "pdf"));
//        BaseFont bfChinese  = BaseFont.createFont("/simsunb.ttf", BaseFont.IDENTITY_H,BaseFont.NOT_EMBEDDED);
        String outPath = "D:\\vmware\\office\\12.docx";
        //如果读取不到字体文件，将字体文件放到templates下，用clsaapath路径读取
        wordConverterToPdf(outPath, outPath.replace("docx", "pdf"), "/STZHONGS.TTF", BaseFont.IDENTITY_H);
        System.out.println("pdf成功");
//        BaseFont bfChinese = BaseFont.createFont( "STSongStd-Light" ,"UniGB-UCS2-H",BaseFont.NOT_EMBEDDED);
    }

    /**
     * 将word文档， 转换成pdf
     * 宋体：STSong-Light
     *
     * @param fontParam1 可以字体的路径，也可以是itextasian-1.5.2.jar提供的字体，比如宋体"STSong-Light"
     * @param fontParam2 和fontParam2对应，fontParam1为路径时，fontParam2=BaseFont.IDENTITY_H，为itextasian-1.5.2.jar提供的字体时，fontParam2="UniGB-UCS2-H"
     * @param tmp        源为word文档， 必须为docx文档
     * @param target     目标输出
     * @throws Exception
     */
    public static void wordConverterToPdf(String tmp, String target,String fontParam1,String fontParam2) {
        InputStream sourceStream = null;
        OutputStream targetStream = null;
        XWPFDocument doc = null;
        try {
            sourceStream = new FileInputStream(tmp);
            targetStream = new FileOutputStream(target);
            doc = new XWPFDocument(sourceStream);
            PdfOptions options = PdfOptions.create();
            //中文字体处理
            options.fontProvider(new IFontProvider() {
                @Override
                public Font getFont(String familyName, String encoding, float size, int style, Color color) {
                    try {
                        BaseFont bfChinese = BaseFont.createFont(fontParam1, fontParam2, BaseFont.NOT_EMBEDDED);
//                        BaseFont bfChinese = BaseFont.createFont( "STSong-Light" ,"UniGB-UCS2-H",BaseFont.NOT_EMBEDDED);

                        Font fontChinese = new Font(bfChinese, size, style, color);
                        if (familyName != null){
                            fontChinese.setFamily(familyName);
                        }
                        return fontChinese;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            });
            PdfConverter.getInstance().convert(doc, targetStream, options);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(doc);
            IOUtils.closeQuietly(targetStream);
            IOUtils.closeQuietly(sourceStream);
        }
    }
}

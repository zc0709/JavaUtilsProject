package com.sl.utils.office.shuiyin.pdf;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * @ProjectName: JavaUtilsProject   需要将lib目录中jar包加到项目中
 * @Package: com.sl.utils.office.shuiyin.pdf
 * @ClassName: PdfAddPicByStr
 * @Author: zc1997
 * @Description:
 * @Date: 2019/12/23 10:39
 * @Version: 1.0
 */
public class PdfAddPicByStr {
    public static void main(String[] args) {
        byte[] byes=null;
        pdfAddWaterMark(byes);
    }
    /**
     * pdf 加水印
     *
     * @return
     */
    public static void pdfAddWaterMark(byte[] byes) {
        String path = "d:" + File.separator+"vmware"+File.separator+"office"+File.separator;
        String fileName = path+UUID.randomUUID().toString() + ".pdf";
        // 待加水印的文件
        PdfReader reader = null;
        PdfStamper stamper = null;
        FileOutputStream os = null;
        try {
//            reader = new PdfReader(byes);
            reader = new PdfReader(path + "12.pdf");
            // 加完水印的文件
            os = new FileOutputStream(fileName);
            stamper = new PdfStamper(reader, os);

            int total = reader.getNumberOfPages() + 1;
            PdfContentByte content;
            BaseFont basefont = null;
            try {
                basefont = BaseFont.createFont("/STZHONGS.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            //这里的字体设置比较关键，这个设置是支持中文的写法
   /*BaseFont base = BaseFont.createFont("STSong-Light",
     "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);// 使用系统字体*/
            PdfContentByte under;
            com.itextpdf.text.Rectangle pageRect = null;

            // 循环对每页插入水印
            for (int i = 1; i < total; i++) {
                pageRect = stamper.getReader().getPageSizeWithRotation(i);
                // 计算水印X,Y坐标
                float x = (float) (pageRect.getWidth() / 1.98);
                float y = (float) (pageRect.getHeight() / 2.8);
                // 获得PDF最底层
                under = stamper.getUnderContent(i);
                // 获得PDF最顶层
//                under = stamper.getOverContent(i);
//                under.saveState();
                // set Transparency
                PdfGState gs = new PdfGState();
                // 设置透明度为0.2
                gs.setFillOpacity(0.5f);
                gs.setStrokeOpacity(0.5f);
                under.saveState();
                under.setGState(gs);
//                under.restoreState();
                under.beginText();
                under.setFontAndSize(basefont, 15);
//                under.setFontAndSize(basefont, pageRect.getHeight() / 17);
                under.setColorFill(BaseColor.RED);

                // 水印文字成45度角倾斜
                System.out.println("width" + pageRect.getWidth());
                System.out.println("height" + pageRect.getHeight());
                System.out.println("x" + x);
                System.out.println("y" + y);
                //右上角
                under.showTextAligned(Element.ALIGN_CENTER, "图片仅供预览", 500, 750, 315);
                //中心
//                under.showTextAligned(Element.ALIGN_CENTER, "图片仅供预览，不可用于商业用途", x, y, 45);
                // 添加水印文字
                under.endText();
                under.setLineWidth(1f);
                under.stroke();
            }
//   returnBytes = baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } finally {
            try {
                stamper.close();
                if (os != null) {
                    os.close();
                }
                if (reader != null) {
                    reader.close();
                }
            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}

package com.sl.utils.office.shuiyin.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * @ProjectName: JavaUtilsProject   需要将lib目录中jar包加到项目中
 * @Package: com.sl.utils.office.shuiyin.pdf
 * @ClassName: PdfAddPicByStrSimple
 * @Author: zc1997
 * @Description:
 * @Date: 2019/12/23 10:03
 * @Version: 1.0
 */
public class PdfAddPicByStrSimple {
    public static void main(String[] args) throws IOException, DocumentException {
        PdfReader reader =null;
        PdfStamper stamp2 =null;
        String path = "d:" + File.separator+"vmware"+File.separator+"office"+File.separator;
//读取原来的pdf
        reader = new PdfReader(path + "1.pdf");
//生成以后的pdf
        stamp2 = new PdfStamper(reader, new FileOutputStream(path + UUID.randomUUID().toString() + ".pdf"));
        int max = reader.getNumberOfPages();
        //max =2;
        // 文字水印
        for (int i = 1; i <= max; i++) {
//            PdfContentByte over = stamp2.getOverContent(i);
            PdfContentByte under = stamp2.getUnderContent(i);
            under.beginText();
            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.EMBEDDED);
            under.setFontAndSize(bf, 16);
            under.setTextMatrix(10, 10);
            under.setColorFill(BaseColor.GRAY);
            under.showTextAligned(Element.ALIGN_RIGHT, "java blog java-er.com ", 550,670, -45);
// 0 0 0 表示左下脚 最后一个0是角度，0表示横着 45 表示斜着
            under.endText();
        }
        stamp2.close();
        reader.close();

    }
}

package com.sl.utils.office.shuiyin.pdf;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * @ProjectName: JavaUtilsProject  需要将lib目录中jar包加到项目中
 * @Package: com.sl.utils.office.shuiyin.pdf
 * @ClassName: PdfAddPicByPicture
 * @Author: zc1997
 * @Description:
 * @Date: 2019/12/23 10:39
 * @Version: 1.0
 */
public class PdfAddPicByPicture {
    public static void main(String[] args) {
        byte[] byes=null;
        pdfAddWaterMark(byes);
    }
    /**
     * pdf 加水印
     *
     * @return
     */
    public static void pdfAddWaterMark(byte[] byes) {
        String path = "d:" + File.separator+"vmware"+File.separator+"office"+File.separator;
        String fileName = path+UUID.randomUUID().toString() + ".pdf";
        byte[] returnBytes = null;
        // 待加水印的文件
        PdfReader reader = null;
        PdfStamper stamper = null;
//  ByteArrayOutputStream baos = null;
        FileOutputStream os = null;
        Image logo = null;
        try {
            logo = Image.getInstance(path+"1.jpg");
        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
//            reader = new PdfReader(byes);
            reader = new PdfReader(path + "1.pdf");
            // 加完水印的文件
//   baos = new ByteArrayOutputStream();
//   stamper = new PdfStamper(reader, baos);
            // 加完水印的文件
            os = new FileOutputStream(fileName);
//            os = new FileOutputStream(courseFile + fileName);
            stamper = new PdfStamper(reader, os);

            int total = reader.getNumberOfPages() + 1;
            PdfContentByte content;
            // BaseFont font = BaseFont.createFont();
            BaseFont basefont = null;
            try {
                basefont = BaseFont.createFont("/STZHONGS.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            //这里的字体设置比较关键，这个设置是支持中文的写法
   /*BaseFont base = BaseFont.createFont("STSong-Light",
     "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);// 使用系统字体*/

   /*//设置透明度
   PdfGState gs = new PdfGState();
   gs.setFillOpacity(1f);
   gs.setStrokeOpacity(1f);*/

            PdfContentByte under;
            Rectangle pageRect = null;

            // 循环对每页插入水印
//            for (int i = 1; i < total; i++) {
//                pageRect = stamper.getReader().getPageSizeWithRotation(i);
//                // 计算水印X,Y坐标
//                float x = (float) (pageRect.getWidth() / 1.98);
//                float y = (float) (pageRect.getHeight() / 2.8);
//                // 获得PDF最顶层
//                under = stamper.getOverContent(i);
//                under.saveState();
//                // set Transparency
//                PdfGState gs = new PdfGState();
//                // 设置透明度为0.2
//                gs.setFillOpacity(1.f);
//                under.setGState(gs);
//                under.restoreState();
//                under.beginText();
//                under.setFontAndSize(basefont, pageRect.getHeight() / 17);
//                under.setColorFill(BaseColor.RED);
//
//                // 水印文字成45度角倾斜
//                System.out.println("width" + pageRect.getWidth());
//                System.out.println("height" + pageRect.getHeight());
//                System.out.println("x" + x);
//                System.out.println("y" + y);
//                under.showTextAligned(Element.ALIGN_CENTER, "图片仅供预览，不可用于商业用途", x, y, 45);
//                // 添加水印文字
//                under.endText();
//                under.setLineWidth(1f);
//                under.stroke();
//            }
            //图片
            for (int i = 1; i < total; i++){
                under = stamper.getUnderContent(i);     // getOverContent ˮӡ������ĸ�ס    getUnderContent ˮӡ�ᱻ���ĵ�ͼƬ��ס
                PdfGState gs = new PdfGState();
                gs.setFillOpacity(0.5f);
                gs.setStrokeOpacity(0.5f);
                under.setGState(gs);
                Rectangle rectangle = reader.getPageSize(i);
                float width = rectangle.getWidth();
                float height = rectangle.getHeight();
                logo.setAbsolutePosition(width/2-logo.getWidth()/2, height/2);
                under.addImage(logo);
            }
//   returnBytes = baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } finally {
            try {
                stamper.close();
                if (os != null) {
                    os.close();
                }
                if (reader != null) {
                    reader.close();
                }
            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}

//package com.sl.utils.office.shuiyin.word;
//
//import java.io.*;
//
//import com.microsoft.schemas.vml.CTTextPath;
//import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
//import org.apache.poi.xwpf.usermodel.XWPFDocument;
//import org.apache.poi.xwpf.usermodel.XWPFHeader;
//import org.apache.poi.xwpf.usermodel.XWPFParagraph;
//
/**
 * poi 3.17版本可以，3.14版本不可用，打开pom.xml中的3.17版本依赖，注释3.14版本
 */
//public class PoiAddPic {
//
//    public static void main(String[] args) throws FileNotFoundException, IOException {
//        String text = "Watermark19";
//        String path = "d:" + File.separator+"vmware"+File.separator+"office"+File.separator;
//        InputStream sourceStream = null;
//        sourceStream = new FileInputStream(path+"1.docx");
//
//        XWPFDocument doc = new XWPFDocument(sourceStream);
////        XWPFDocument doc= new XWPFDocument();
//
//        // the body content
//        XWPFParagraph paragraph = doc.createParagraph();
////        XWPFRun run=paragraph.createRun();
////        run.setText("The Body:");
//
//        // create header-footer
//        XWPFHeaderFooterPolicy headerFooterPolicy = doc.getHeaderFooterPolicy();
//        XWPFHeaderFooterPolicy headerFooterPolicyTemp = headerFooterPolicy;
//        // get the default header
//        // Note: createWatermark also sets FIRST and EVEN headers 
//        // but this code does not updating those other headers
//        XWPFHeader header = headerFooterPolicy.getHeader(XWPFHeaderFooterPolicy.DEFAULT);
//        if (header == null){
//            headerFooterPolicy = doc.createHeaderFooterPolicy();
//            // create default Watermark - fill color black and not rotated
//            headerFooterPolicy.createWatermark(text);
//            header = headerFooterPolicy.getHeader(XWPFHeaderFooterPolicy.DEFAULT);
//        }
//
//        //2.paragraph中的第二个才起作用，不知为何
//        paragraph = header.getParagraphArray(1);
//        // get com.microsoft.schemas.vml.CTShape where fill color and rotation is set
//        org.apache.xmlbeans.XmlObject[] xmlobjects = paragraph.getCTP().getRArray(0).getPictArray(0).selectChildren(
//                new javax.xml.namespace.QName("urn:schemas-microsoft-com:vml", "shape"));
//        if (xmlobjects.length > 0) {
//            com.microsoft.schemas.vml.CTShape ctshape = (com.microsoft.schemas.vml.CTShape)xmlobjects[0];
//            // set fill color
//            ctshape.setFillcolor("#d8d8d8");
//            // set rotation
//            ctshape.setStyle("position:absolute;margin-left:0;margin-top:0;width:100pt;height:50pt;z-index:-251654144;mso-wrap-edited:f;mso-position-horizontal:right;mso-position-horizontal-relative:margin;mso-position-vertical:top;mso-position-vertical-relative:margin;rotation:45");
//            if (headerFooterPolicyTemp!=null){
//                ctshape.removeTextpath(0);
//                CTTextPath textPath = ctshape.addNewTextpath();
//                textPath.setStyle("font-family:&quot;Cambria&quot;;font-size:1pt");
//                textPath.setString(text);
//            }
//            System.out.println(ctshape);
//        }
//        doc.write(new FileOutputStream(path+"12.docx"));
//        doc.close();
//
//    }
//
//}

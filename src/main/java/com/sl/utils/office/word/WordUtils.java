package com.sl.utils.office.word;

import com.sl.utils.date.DateUtils;
import com.sl.utils.freemark.FreeMarkUtils;
import org.apache.commons.io.FileUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * docx、doc文档生成工具类  (改变后缀名即可)
 * 在使用制作模板的过程中如果模板中有图片那就保留图片，注意[Content_Types].xml和document.xml.rels文档
 * 如果模板中没有图片 则不需要设置[Content_Types].xml和document.xml.rels
 * 由于word模板的个性化 所以 每次做模板都要重新覆盖原来的模板
 *
 *
 *
 * gaoxueyong
 */
public class WordUtils {

    private final static String SEPARATOR = File.separator;
    private final static String SUFFIX_DOCX = "docx";
    private final static String SUFFIX_DOC = "doc";


    /*
     * @param dataMap               参数数据
     * @param docxTemplateFile      docx模主板名称
     * @param xmlDocument           docx中document.xml模板文件  用来存在word文档的主要数据信息
     * @param xmlDocumentXmlRels    docx中document.xml.rels 模板文件  用来存在word文档的主要数据配置 包括图片的指向
     * @param xmlContentTypes       docx中 [Content_Types].xml 模板文件 用来配置 docx文档中所插入图片的类型 如 png、jpeg、jpg等
     * @param xmlHeader             docx中 header1.xml 模板文件 用来配置docx文档的页眉文件
     * @param templatePath          模板存放路径 如 /templates/
     * @param outputFileTempPath    所生成的docx文件的临时路径文件夹 如果 temp/20180914051811/
     * @param outputFileName        所生成的docx文件名称  如  xxx.docx  或  xxx.doc
     * */
    public static void createDocx(Map dataMap, String docxTemplateFile, String xmlDocument, String xmlDocumentXmlRels,
                                  String xmlContentTypes, String xmlHeader,String xmlHeader2,String xmlHeader3, String templatePath,
                                  String outputFileTempPath, String outputFileName) throws Exception {

        URL basePath = WordUtils.class.getClassLoader().getResource("");
        String base = basePath.getPath().toString();
        //解决路径包含中文的情况
        String packagePath = URLDecoder.decode(base,"utf-8");
//        System.out.println("basePath.getPath() ==> " + basePath.getPath());
        String realTemplatePath = packagePath + templatePath;
        //临时文件产出的路径
        String outputPath = packagePath + outputFileTempPath;
        String lastFilePath = outputFileTempPath + outputFileName;
        List<String> delFileList = new ArrayList<>();
        ZipFile zipFile = null;
        ZipOutputStream zipout = null;
        InputStream in=null;
        try {


            //获取 document.xml.rels 输入流
            String xmlDocumentXmlRelsComment = FreeMarkUtils.getFreemarkerContent(dataMap, xmlDocumentXmlRels, templatePath);
            ByteArrayInputStream documentXmlRelsInput =
                    new ByteArrayInputStream(xmlDocumentXmlRelsComment.getBytes());

            //获取 header1.xml 输入流
            ByteArrayInputStream headerInput = FreeMarkUtils.getFreemarkerContentInputStream(dataMap, xmlHeader, templatePath);
            ByteArrayInputStream header2Input = FreeMarkUtils.getFreemarkerContentInputStream(dataMap, xmlHeader2, templatePath);
            ByteArrayInputStream header3Input = FreeMarkUtils.getFreemarkerContentInputStream(dataMap, xmlHeader3, templatePath);

            //获取 [Content_Types].xml 输入流
            ByteArrayInputStream contentTypesInput = FreeMarkUtils.getFreemarkerContentInputStream(dataMap, xmlContentTypes, templatePath);
            //获取 [Content_Types].xml 输入流

            //读取 document.xml.rels  文件 并获取rId 与 图片的关系 (如果没有图片 此文件不用编辑直接读取就行了)
            Document document = DocumentHelper.parseText(xmlDocumentXmlRelsComment);
            // 获取根节点
            Element rootElt = document.getRootElement();
            // 获取根节点下的子节点head
            Iterator iter = rootElt.elementIterator();
            List<Map<String, String>> picList = (List<Map<String, String>>) dataMap.get("modelList");

            // 遍历Relationships节点
            while (iter.hasNext()) {
                Element recordEle = (Element) iter.next();
                String id = recordEle.attribute("Id").getData().toString();
                String target = recordEle.attribute("Target").getData().toString();
                if (target.indexOf("media") == 0) {
                    for (Map<String, String> picMap : picList) {
                        if (target.endsWith(picMap.get("name"))) {
                            picMap.put("rId", id);
                        }
                    }
                }
            }
            //覆盖原来的picList;
            dataMap.put("modelList", picList);

            //获取 document.xml 输入流
            ByteArrayInputStream documentInput = FreeMarkUtils.getFreemarkerContentInputStream(dataMap, xmlDocument, templatePath);
            //获取 document.xml 输入流


//            System.out.println("base_path_template+SEPARATOR+docxTemplate===="+base_path_template+SEPARATOR+docxTemplate);
            File docxFile = new File(realTemplatePath + SEPARATOR + docxTemplateFile);
            if (!docxFile.exists()) {
                docxFile.createNewFile();
            }

            zipFile = new ZipFile(docxFile);
            Enumeration<? extends ZipEntry> zipEntrys = zipFile.entries();
            File tempPath = new File(outputPath);
            //如果输出目标文件夹不存在，则创建
            if (!tempPath.exists()) {
                tempPath.mkdirs();
            }
            zipout = new ZipOutputStream(new FileOutputStream(outputPath + outputFileName));


            //------------------覆盖文档------------------
            int len = -1;
            byte[] buffer = new byte[1024];
            while (zipEntrys.hasMoreElements()) {
                ZipEntry next = zipEntrys.nextElement();
                InputStream is = zipFile.getInputStream(next);
                if (next.toString().indexOf("media") < 0) {
                    // 把输入流的文件传到输出流中 如果是word/document.xml由我们输入
                    zipout.putNextEntry(new ZipEntry(next.getName()));
//                    System.out.println("next.getName()>>>" + next.getName() + "  next.isDirectory()>>>" + next.isDirectory());
                    //写入图片配置类型
                    if (next.getName().equals("[Content_Types].xml")) {
                        if (contentTypesInput != null) {
                            while ((len = contentTypesInput.read(buffer)) != -1) {
                                zipout.write(buffer, 0, len);
                            }
                            contentTypesInput.close();
                        }

                    } else if (next.getName().indexOf("document.xml.rels") > 0) {
                        //写入填充数据后的主数据配置信息
                        if (documentXmlRelsInput != null) {
                            while ((len = documentXmlRelsInput.read(buffer)) != -1) {
                                zipout.write(buffer, 0, len);
                            }
                            documentXmlRelsInput.close();
                        }
                    } else if ("word/document.xml".equals(next.getName())) {
                        //写入填充数据后的主数据信息
                        if (documentInput != null) {
                            while ((len = documentInput.read(buffer)) != -1) {
                                zipout.write(buffer, 0, len);
                            }
                            documentInput.close();
                        }

                    } else if ("word/header1.xml".equals(next.getName())) {
                        //写入填充数据后的页眉信息
                        if (headerInput != null) {
                            while ((len = headerInput.read(buffer)) != -1) {
                                zipout.write(buffer, 0, len);
                            }
                            headerInput.close();
                        }

                    }else if ("word/header2.xml".equals(next.getName())){
                        //写入填充数据后的页眉信息
                        if (header2Input != null) {
                            while ((len = header2Input.read(buffer)) != -1) {
                                zipout.write(buffer, 0, len);
                            }
                            header2Input.close();
                        }
                    }else if ("word/header3.xml".equals(next.getName())){
                        //写入填充数据后的页眉信息
                        if (header3Input != null) {
                            while ((len = header3Input.read(buffer)) != -1) {
                                zipout.write(buffer, 0, len);
                            }
                            header3Input.close();
                        }
                    }
                    else {
                        while ((len = is.read(buffer)) != -1) {
                            zipout.write(buffer, 0, len);
                        }
                        is.close();
                    }

                }

            }
            //------------------覆盖文档------------------

            //------------------写入新图片------------------
            len = -1;
            if (picList != null && !picList.isEmpty()) {
                for (Map<String, String> pic : picList) {
                    ZipEntry next = new ZipEntry("word" + SEPARATOR + "media" + SEPARATOR + pic.get("name"));
                    zipout.putNextEntry(new ZipEntry(next.toString()));
                    in = new FileInputStream(pic.get("path"));
                    while ((len = in.read(buffer)) != -1) {
                        zipout.write(buffer, 0, len);
                    }
                    in.close();
                }
            }
//            return lastFilePath;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("生成docx文件失败！");
        }finally {
            zipFile.close();
            zipout.close();
            if (null != in){
                in.close();
            }
        }

    }


    /**
     * 递归删除文件夹 ，优先使用此方法，io自带工具类org.apache.commons.io.FileUtils#deleteDirectory
     *
     * @param dir
     */
    public static void delDir(String dir) {
        try {
            File file = new File(dir);
            FileUtils.deleteDirectory(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 删除文件
     *
     * @param listFiles
     */
    public static void delFiles(List<String> listFiles) {
        try {
            if (listFiles != null && !listFiles.isEmpty()) {
                for (String file_temp_path : listFiles) {
                    File file_temp = new File(file_temp_path);
                    if (file_temp.exists()) {
                        file_temp.delete();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    /**
     * 递归删除文件夹
     *
     * @param dir
     */
    public static void delFiles(String dir) {
        try {
            File file = new File(dir);
            if(!file.exists()){
                return;
            }
            if(file.isFile() || file.list()==null) {
                file.delete();
                System.out.println("删除了"+file.getName());
            }else {
                File[] files = file.listFiles();
                for(File a:files) {
                    a.delete();
                }
                file.delete();
                System.out.println("删除了"+file.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        URL basePath = WordUtils.class.getClassLoader().getResource("");
        //获取jar包路径
//        String userDir = System.getProperty("user.dir");
//        String tempPath = userDir + SEPARATOR + "tempLicense" + SEPARATOR;
//        File file = new File(tempPath);
//        if (!file.exists()){
//            file.mkdirs();
//        }
        //写文件时出现权限问题解决方法 开启文件可写
//        File file = new File(filePath);
//        file.setWritable(true);

//        System.out.println("basePath.getPath() ==> " + basePath.getPath());
        String picPath = basePath.getPath() + SEPARATOR + "templates" + SEPARATOR;

        Map<String, Object> dataMap = new HashMap<>();
//        页眉
        dataMap.put("ymdhis", DateUtils.getCurrentTime_yyyyMMddHHmmss());
//        水印
        dataMap.put("waterPic", "水印模板");
//      图片类型
        List<String> picTypes = new ArrayList<>();
        picTypes.add("jpg");
        dataMap.put("mdlTypes", picTypes);
//        文档标题
        dataMap.put("title", "文档的标题");
        dataMap.put("reportUnit", "文档的单位  ");
        dataMap.put("reportTypeDate", "文档的报告周期");

//        模块内容列表
        List<Map<String, Object>> picList = new ArrayList<>();
//        模块内容列表
        List<Map<String, Object>> picList2 = new ArrayList<>();
        List<Map<String, Object>> picList3 = new ArrayList<>();

        Map<String, Object> picMap = new HashMap<>();
        Map<String, Object> picMap2 = new HashMap<>();
        Map<String, Object> picMap3 = new HashMap<>();
        // 要按顺序
        picMap.put("path", picPath + "pic1.jpg");
        picMap.put("name", "pic1.jpg");
        picMap.put("modelTitle", "模块1标题");
        picMap.put("modelDataSource", "模块1来源：美团");
        picMap.put("modelShowContent", "模块1内容sasasaaaaaaaaaa");
        picMap2 = new HashMap<>();
        picMap2.put("content","报告内容");
        picMap2.put("header","报告表头");
        picList2.add(picMap2);
        picMap.put("modelList2",picList2);
        picList.add(picMap);

        picMap = new HashMap<>();
        picMap.put("path", picPath + "pic2.jpg");
        picMap.put("name", "pic2.jpg");
        picMap.put("modelTitle", "模块2标题");
        picMap.put("modelDataSource", "模块2来源：美团");
        picMap.put("modelShowContent", "模块2内容sasasaaaaaaaaaa");
        picMap2 = new HashMap<>();
        picMap2.put("content","报告内容");
        picMap2.put("header","报告表头");
        picMap3 = new HashMap<>();
        picMap3.put("content","报告内容数据");
        picMap3.put("header","报告表头数据");
        picList3.add(picMap3);
        picMap2.put("modelList3",picList3);
        picList2.add(picMap2);
        picMap.put("modelList2",picList2);
        picList.add(picMap);

        picMap = new HashMap<>();
        picMap.put("path", picPath + "pic3.jpg");
        picMap.put("name", "pic3.jpg");
        picMap.put("modelTitle", "模块3标题");
        picMap.put("modelDataSource", "模块3来源：美团");
        picMap.put("modelShowContent", "模块3内容sasasaaaaaaaaaa");

        picMap2 = new HashMap<>();
        picMap2.put("content","报告内容");
        picMap2.put("header","报告表头");
        picList2.add(picMap2);
        picMap.put("modelList2",picList2);


        picList.add(picMap);
        dataMap.put("modelList", picList);

        String timeStr = DateUtils.getCurrentTime_yyyyMMddHHmmssSSS();
        String docxTemplateFile = "docxTemplates.docx";
        //带水印的模板
        String docxTemplatesWithWaterPic = "docxTemplatesWithWaterPic.docx";
        String xmlDocument = "document.xml";
        String xmlDocumentXmlRels = "document.xml.rels";
        String xmlContentTypes = "[Content_Types].xml";
        //可以用来修改页眉的一些信息
        String xmlHeader = "header1.xml";
        String xmlHeader2 = "header2.xml";
        String xmlHeader3 = "header3.xml";
        String templatePath = SEPARATOR + "templates" + SEPARATOR;
        String outputFileTempPath = "temp" + SEPARATOR + timeStr + SEPARATOR;
        String outputFileName = timeStr + "."+SUFFIX_DOCX;
//        String outputFileName = timeStr + "."+SUFFIX_DOC;

        /*
        * @param dataMap               参数数据
        * @param docxTemplateFile      docx模主板名称
        * @param xmlDocument           docx中document.xml模板文件  用来存在word文档的主要数据信息
        * @param xmlDocumentXmlRels    docx中document.xml.rels 模板文件  用来存在word文档的主要数据配置 包括图片的指向
        * @param xmlContentTypes       docx中 [Content_Types].xml 模板文件 用来配置 docx文档中所插入图片的类型 如 png、jpeg、jpg等
        * @param xmlHeader             docx中 header1.xml 模板文件 用来配置docx文档的页眉文件
        * @param templatePath          模板存放路径 如 /templates/
        * @param outputFileTempPath    所生成的docx文件的临时路径文件夹 如果 temp/20180914051811/
        * @param outputFileName        所生成的docx文件名称  如  xxx.docx 或  xxx.doc
        * */
        try {
            //不带水印
//            createDocx(dataMap, docxTemplateFile, xmlDocument, xmlDocumentXmlRels, xmlContentTypes,
//                    xmlHeader, templatePath, outputFileTempPath, outputFileName);
            //水印
            createDocx(dataMap, docxTemplatesWithWaterPic, xmlDocument, xmlDocumentXmlRels, xmlContentTypes,
                    xmlHeader,xmlHeader2,xmlHeader3, templatePath, outputFileTempPath, outputFileName);
            //删除文件夹
//            delDir();

//            String xmlDocumentXmlRelsComment = FreeMarkUtils.getFreemarkerContent(dataMap,xmlDocumentXmlRels,SEPARATOR + "templates" );
//            System.out.println(xmlDocumentXmlRelsComment);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

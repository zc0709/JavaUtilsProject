package com.sl.utils.serve;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * 自定义需要校验的License参数
 *
 * @author zhangc
 * @date 2020/8/31
 * @since 1.0.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ServerInfos implements Serializable {

    private static final long serialVersionUID = 8600137500316662317L;
    /**
     * 可被允许的IP地址
     */
    private List<String> ipAddress;
    /**
     * 可被允许的MAC地址
     */
    private List<String> macAddress;
    /**
     * 可被允许的CPU序列号
     */
    private String cpuSerial;
    /**
     * 可被允许的主板序列号
     */
    private String mainBoardSerial;
}

package com.sl.utils.serve;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * @author ：zhangC
 * @date ：Created in 2020/9/14 16:06
 * @description：获取服务器硬件信息
 * @version: $1.0
 */
@Slf4j
public class ServerInfoUtils {

    /**
     * 测试
     *
     * @param args
     */
    public static void main(String[] args) {
        ServerInfos serverInfos = getServerInfos("windows");

    }

    public static ServerInfos getServerInfos(String osName) {
        //操作系统类型
        if (StringUtils.isBlank(osName)) {
            osName = System.getProperty("os.name");
        }
        osName = osName.toLowerCase();

        AbstractServerInfos abstractServerInfos = null;

        //根据不同操作系统类型选择不同的数据获取方法
        if (osName.startsWith("windows")) {
            abstractServerInfos = new WindowsServerInfos();
        } else if (osName.startsWith("linux")) {
            abstractServerInfos = new LinuxServerInfos();
        } else {//其他服务器类型
            abstractServerInfos = new LinuxServerInfos();
        }
        return abstractServerInfos.getServerInfos();
    }

}

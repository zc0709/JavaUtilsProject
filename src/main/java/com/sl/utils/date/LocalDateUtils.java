package com.sl.utils.date;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;

/**
 * @author : zhouxd
 * @date : 2019/11/6 11:41
 */
public class LocalDateUtils {


    public static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static DateTimeFormatter dateFormatterCn = DateTimeFormatter.ofPattern("yyyy年MM月dd日");

    /**
     * 获取月份
     */
    public static int getMonth(String dateStr, String formatter) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(formatter);
        LocalDate date = LocalDate.parse(dateStr, dateTimeFormatter);
        return date.getMonth().getValue();
    }

    /**
     * 获取月份
     */
    public static String getMonthStr(String dateStr, String formatter) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(formatter);
        LocalDate date = LocalDate.parse(dateStr, dateTimeFormatter);
        int month = date.getMonth().getValue();
        if (month < Month.OCTOBER.getValue()) {
            return "0" + month;
        }
        return String.valueOf(month);
    }

    /**
     * 计算季度
     */
    public static int getQuarter(String dateStr, String formatter) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(formatter);
        LocalDate date = LocalDate.parse(dateStr, dateTimeFormatter);
        return getQuarter(date);
    }

    /**
     * 计算季度
     */
    public static int getQuarter(LocalDate date) {
        int month = date.getMonth().getValue();
        int quarter = 0;
        if (month >= 1 && month <= 3) {
            //第一季度
            return 1;
        } else if (month >= 4 && month <= 6) {
            //第二季度
            quarter = 2;
        } else if (month >= 7 && month <= 9) {
            //第三季度
            quarter = 3;
        } else if ((month >= 10 && month <= 12)) {
            //第四季度
            quarter = 4;
        }
        return quarter;
    }

    /**
     * 获取一个月的最后一天
     * @param dateStr 日期
     * @return 最后一天
     */
    public static String getLastDayInMonth(String dateStr) {
        LocalDate date = LocalDate.parse(dateStr, LocalDateUtils.dateFormatter);
        LocalDate eDate = date.with(TemporalAdjusters.lastDayOfMonth());
        return eDate.format(LocalDateUtils.dateFormatter);
    }
    /**
     * 获取当前日期的字符串(毫秒) 如
     * @return
     */
    public static String getCurrentTimeMil(){
        LocalDateTime time = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
        String dateTime = time.format(dateTimeFormatter);
        return dateTime;
    }
    /**
     * 获取当前日期的字符串 如  20171116
     * @return
     */
    public static String getCurrentTimeSss(){
        LocalDateTime time = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        String dateTime = time.format(dateTimeFormatter);
        return dateTime;
    }
    /**
     * 获取当前日期的字符串 如  20171116
     * @return
     */
    public static String getDateTime(LocalDateTime time,String format){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        String dateTime = time.format(dateTimeFormatter);
        return dateTime;
    }
    /**
     * 获取当前日期的字符串 如  20171116
     * @return
     */
    public static LocalDateTime getLocalDateTime(String time,String format){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        LocalDateTime dateTime = LocalDateTime.parse(time,dateTimeFormatter);
        return dateTime;
    }
    public static LocalDateTime date2localDateTime(String date){
        String localDate = date +" 00:00:00";
        LocalDateTime localDateTime = getLocalDateTime(localDate,"yyyy-MM-dd HH:mm:ss");
        return localDateTime;
    }
}

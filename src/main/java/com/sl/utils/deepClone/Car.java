package com.sl.utils.deepClone;

import lombok.ToString;

import java.io.Serializable;

/**
 * @ProjectName: JavaUtilsProject
 * @Package: com.sl.utils.deepCopy
 * @ClassName: Car
 * @Author: zc1997
 * @Description: 汽车
 * @Date: 2020/2/14 11:51
 * @Version: 1.0
 */
@ToString
public class Car implements Serializable {
    private static final long serialVersionUID = -5713945027627603702L;

    private String brand;       // 品牌
    private int maxSpeed;       // 最高时速

    public Car() {
    }

    public Car(String brand, int maxSpeed) {
        this.brand = brand;
        this.maxSpeed = maxSpeed;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        return "Car [brand=" + brand + ", maxSpeed=" + maxSpeed + "]";
    }

}
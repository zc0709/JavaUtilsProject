package com.sl.utils.deepClone;

/**
 * @ProjectName: JavaUtilsProject
 * @Package: com.sl.utils.deepCopy
 * @ClassName: CloneTest
 * @Author: zc1997
 * @Description: 深度克隆测试
 * @Date: 2020/2/14 11:52
 * @Version: 1.0
 */
class CloneTest {
    /**
     * 如何实现对象克隆？
     * 答：有两种方式：
     *   1). 实现Cloneable接口并重写Object类中的clone()方法；
     *   2). 实现Serializable接口，通过对象的序列化和反序列化实现克隆，可以实现真正的深度克隆，代码如下。
     * 注意：基于序列化和反序列化实现的克隆不仅仅是深度克隆，更重要的是通过泛型限定，可以检查出要克隆的对象是否支持序列化，
     *    这项检查是编译器完成的，不是在运行时抛出异常，这种是方案明显优于使用Object类的clone方法克隆对象。
     *    让问题在编译的时候暴露出来总是好过把问题留到运行时。
     * @param args
     */
    public static void main(String[] args) {
        try {
            Person p1 = new Person("Hao LUO", 33, new Car("Benz", 300));
            Person p2 = DeepCloneUtil.clone(p1);   // 深度克隆
            p2.getCar().setBrand("BYD");
            // 修改克隆的Person对象p2关联的汽车对象的品牌属性
            // 原来的Person对象p1关联的汽车不会受到任何影响
            // 因为在克隆Person对象时其关联的汽车对象也被克隆了
            System.out.println(p1);
            System.out.println(p2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
package com.sl.utils.enumClass;


/**
 * @ProjectName: travel-report
 * @Package: com.zjhcsoft.travel.constants
 * @ClassName: HolidayEnum
 * @Author: zhangchao
 * @Description: 节假日枚举
 * @Date: 2020/1/3 11:18
 * @Version: 1.0
 */
public enum HolidayEnum {
    /**
     * 元旦
     */
    NewYear(1, "元旦"),
    /**
     * 春节
     */
    SPRING_FESTIVAL(2, "春节"),
    /**
     * 清明节
     */
    TOMB_SWEEPING_DAY(3, "清明节"),
    /**
     * 劳动节
     */
    LABOR_DAY(4, "劳动节"),
    /**
     * 端午节
     */
    THE_DRAGON_BOAT_FESTIVAL(5, "端午节"),
    /**
     * 中秋节
     */
    MID_AUTUMN_FESTIVAL(6, "中秋节"),
    /**
     * 国庆节
     */
    NATIONAL_DAY(7, "国庆节");

    private int value;
    private String name;

    HolidayEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getValueByName(String name) {
        int value = 0;
        for (HolidayEnum s : HolidayEnum.values()) {
            if (s.getName().equals(name)) {
                value = s.getValue();
            }
        }
        return value;
    }

    public static HolidayEnum getByValue(int value) {
        for (HolidayEnum s : HolidayEnum.values()) {
            if (s.getValue() == value) {
                return s;
            }
        }
        return null;
    }
}

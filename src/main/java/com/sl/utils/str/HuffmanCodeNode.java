package com.sl.utils.str;

/**
 * @author ：zhangC
 * @date ：Created in 2021/1/25 11:15
 * @description：
 * @version: $2.0
 */
// 定义节点类
class HuffmanCodeNode implements Comparable<HuffmanCodeNode>{
    private Byte character;
    private int value;
    private hardwareInfo.utils.HuffmanCodeNode leftNode;
    private hardwareInfo.utils.HuffmanCodeNode rightNode;

    // 前序遍历
    public void preOrder() {
        System.out.println(this);
        if (this.leftNode != null) {
            this.leftNode.preOrder();
        }
        if (this.rightNode != null) {
            this.rightNode.preOrder();
        }
    }

    public HuffmanCodeNode(Byte character, int value) {
        this.character = character;
        this.value = value;
    }

    public Byte getCharacter() {
        return character;
    }

    public void setCharacter(byte character) {
        this.character = character;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public hardwareInfo.utils.HuffmanCodeNode getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(hardwareInfo.utils.HuffmanCodeNode leftNode) {
        this.leftNode = leftNode;
    }

    public hardwareInfo.utils.HuffmanCodeNode getRightNode() {
        return rightNode;
    }

    public void setRightNode(hardwareInfo.utils.HuffmanCodeNode rightNode) {
        this.rightNode = rightNode;
    }

    @Override
    public String toString() {
        return "HuffmanCodeNode{" +
                "character=" + character +
                ", value=" + value +
                '}';
    }

    @Override
    public int compareTo(hardwareInfo.utils.HuffmanCodeNode o) {
        // 升序排序
        return this.value - o.value;
    }
}

package com.sl.utils.pacong;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sl.utils.file.FileUtils;
import com.sl.utils.http.NewHttpUtils;
import com.sl.utils.office.excel.ExcelUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author ：zhangC
 * @date ：Created in 2020/11/19 14:37
 * @description：
 * @version: $2.0
 */
@Service
public class Detail {
    @Autowired
    NewHttpUtils newHttpUtils;

    public static void main(String[] args) {
        //1.获取项目列表信息 -------------

//        String url = "https://keyscoops.com/api/v1/web/project/findDetail.do/";
//        String fileDir = "C:\\soft\\mingdu\\project\\";
//        String jsonFile = FileUtils.readJsonFile("C:\\Users\\mdtech\\Desktop\\化工.json");
//        List<ProjectInfo> list = JSONObject.parseArray(jsonFile, ProjectInfo.class);
//        try {
//            exportExcel(list);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//2.获取联系方式 -------------
        getDetails();
    }

    public static void getDetails() {
        String url = "https://keyscoops.com/api/v1/web/project/findDetail.do/";
        String fileDir = "C:\\soft\\mingdu\\project\\";
        String jsonFile = FileUtils.readJsonFile("C:\\Users\\mdtech\\Desktop\\id.json");
        List<ProjectId> list = JSONObject.parseArray(jsonFile, ProjectId.class);
        for (int i = 0; i < list.size(); i++) {
            Integer projectId = list.get(i).getProjectId();
            String projectUrl = url + projectId;
            String response = NewHttpUtils.sendGetRequest(projectUrl, null,
                    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MDU3ODIwMzYsInVzZXJuYW1lIjoia3ltZDIwMjAifQ.1FPf8BrvnyYE211hhrPoksBsLYMUkFl-A4ZkXsHzXl0");
            String fileName = fileDir + projectId + ".json";
            JSONObject json = JSONObject.parseObject(response);
            Integer code = json.getInteger("code");
            if (code == 200) {
                FileUtils.strToFile(response, fileName);
                JSONObject data = json.getJSONObject("data");
                JSONArray units = data.getJSONArray("units");
                if (null != units) {
                    for (int i1 = 0; i1 < units.size(); i1++) {
                        JSONObject o = units.getJSONObject(i1);
                        String contacts = o.getString("contacts");
                        System.out.println("ID：" + projectId + "\n Contacts： " + contacts);
                    }
                }
            }


        }
    }

    // "projectId": 154775,
//                "pid": 154436,
//                "versionId": 2,
//                "projectName": "河南修善堂药业有限公司年产1200吨茶剂、2500吨片剂、1100吨胶囊剂、8000万贴剂保健品项目",
//                "projectPhase": "报批立项",
//                "subIndustryName": "化学原料药及制剂",
//                "investMoneyAmount": "1.65亿",
//                "recommendablePrjFlag": false,
//                "procurementPrjFlag": false,
//                "commissionFlag": 0,
//                "deviceNames": null,
//                "procurementStatus": "未采购",
//                "provinceName": "河南",
//                "projectCraftLine": "片剂：原料-粉碎-筛选-配料-制粒-干燥-整粒-总混-压片-分装-成品。\n贴剂：原材料（无纺布、水刺布、医用亚敏胶）外购-原料检验-称量配料-溶胶炼制-混合成膏-挤压-分切-切片-成型-产品中间检验-合片-内包-外包-成品检验-入库-出厂。",
//                "releaseTime": "2020-11-04",
//                "viewProjectToday": false
    public static void exportExcel(List<ProjectInfo> list) throws IOException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYYMMDDhhmmss");
        String now = dateFormat.format(new Date());
        //导出文件路径
        //文件名 企业名称、地址、工期、联系人、联系电话,预算
        String[] title = {"序号", "项目id", "主键id", "版本ID", "项目名称", "项目进度", "子行业名称", "投资金额", "推荐的项目标志", "采购项目标志",
                "佣金标志", "设备名", "采购状态", "省名", "项目工艺线", "推出日期"};
        String sheelName = "化工项目信息";
        String fileName = "化工项目数据_" + now + ".xlsx";
        String suffix = "";
        String filePath = "C:" + File.separator + "soft" + File.separator + "mingdu" + File.separator + "project" + File.separator + "excelExport" + File.separator;
        //需要导出的数据
        ExcelUtils.objectExportExcelXlsx(title, sheelName, list, fileName, filePath);
    }
}

package com.sl.utils.pacong;

import lombok.Data;

/**
 * @author ：zhangC
 * @date ：Created in 2020/11/19 14:43
 * @description：
 * @version: $2.0
 */
@Data
public class ProjectId {

    private Integer projectId;
}

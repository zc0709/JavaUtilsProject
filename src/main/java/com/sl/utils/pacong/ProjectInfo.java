package com.sl.utils.pacong;

import lombok.Data;

import java.util.List;

/**
 * @author ：zhangC
 * @date ：Created in 2020/11/19 15:40
 * @description：
 * @version: $2.0
 */
@Data
public class ProjectInfo {
    private Integer projectId;
    private Integer pid;
    private Integer versionId;
    private String provinceName;
    private Integer commissionFlag;
    private Boolean recommendablePrjFlag;
    private Boolean procurementPrjFlag ;
    private Boolean viewProjectToday;
    private List<String> deviceNames;
    private String projectCraftLine;
    private String procurementStatus;
    private String releaseTime;
    private String projectName;
    private String projectPhase;
    private String subIndustryName;
    private String investMoneyAmount;
}

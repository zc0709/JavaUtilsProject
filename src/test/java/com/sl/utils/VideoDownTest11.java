package com.sl.utils;


import com.alibaba.fastjson.JSONObject;
import com.sl.utils.http.HttpUtils;

import java.io.*;
import java.util.Arrays;

/**
 * Description(这里用一句话描述这个方法的作用)
 * author: Gao xueyong
 * Create at: 2019/7/28 6:39 PM
 */
public class VideoDownTest11 {

    public static String urlHead = "https://3atvplay.com";
    public static String filePath = "/Users/gaoxueyong/Documents/mergeVideo/ggggggggg.ts";

    public static void main(String[] args) throws IOException {


        File resultFile = new File(filePath);
        if (!resultFile.exists()) {
            try {
                resultFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String temp = "/Users/gaoxueyong/Documents/mergeVideo/";
        String[] tempAry = {"09_l002257rykg.320091.1.ts","025_l002257rykg.320091.1.ts","010_l002257rykg.320091.1.ts"};
        BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(resultFile));
        BufferedInputStream inputStream = null;
        int bufSize = 1024;
        byte[] buffer = new byte[bufSize];
        for(String url:tempAry){
            String result = HttpUtils.sendGet(urlHead + url, null);
            inputStream = new BufferedInputStream(new ByteArrayInputStream(result.getBytes()));
            int readcount;
            while ((readcount = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, readcount);
            }
            inputStream.close();
        }
        outputStream.close();

    }


    public static String[] getUrlArray() {

        String str = "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149000.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149001.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149002.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149003.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149004.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149005.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149006.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149007.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149008.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149009.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149010.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149011.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149012.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149013.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149014.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149015.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149016.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149017.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149018.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149019.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149020.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149021.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149022.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149023.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149024.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149025.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149026.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149027.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149028.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149029.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149030.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149031.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149032.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149033.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149034.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149035.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149036.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149037.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149038.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149039.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149040.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149041.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149042.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149043.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149044.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149045.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149046.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149047.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149048.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149049.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149050.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149051.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149052.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149053.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149054.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149055.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149056.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149057.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149058.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149059.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149060.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149061.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149062.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149063.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149064.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149065.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149066.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149067.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149068.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149069.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149070.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149071.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149072.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149073.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149074.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149075.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149076.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149077.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149078.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149079.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149080.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149081.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149082.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149083.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149084.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149085.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149086.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149087.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149088.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149089.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149090.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149091.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149092.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149093.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149094.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149095.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149096.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149097.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149098.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149099.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149100.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149101.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149102.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149103.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149104.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149105.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149106.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149107.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149108.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149109.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149110.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149111.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149112.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149113.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149114.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149115.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149116.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149117.ts," +
                "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149118.ts";
//        str = "/20190721/AJxxesFC/1565kb/hls/7Fq6Ed6149000.ts";
        return str.split(",");
    }
}
